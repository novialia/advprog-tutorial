package hello;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class CvController {
    @GetMapping("/cv")
    public String cv(@RequestParam(name = "visitor", required = false)
                             String visitor, Model model) {
        if (visitor != null) {
            model.addAttribute("visitor", visitor + ", I hope you interested to hire me");
        } else {
            model.addAttribute("visitor", "This is my CV");
        }
        model.addAttribute("name","Novianti Aliasih");
        model.addAttribute("birthInfo","Jakarta, 24 November 1997");
        model.addAttribute("address",
                "Jalan Cucakrawa, Bukit Duri, Tebet, Jakarta");
        model.addAttribute("sma","SMA Negeri 54 Jakarta");
        model.addAttribute("smaYear","2012-2015");
        model.addAttribute("univ","University of Indonesia");
        model.addAttribute("univYear","2015-Now");
        return "cv";
    }
}
