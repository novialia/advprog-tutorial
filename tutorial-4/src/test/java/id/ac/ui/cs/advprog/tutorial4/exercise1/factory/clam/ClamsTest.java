package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

import static org.junit.Assert.assertTrue;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import org.junit.Before;
import org.junit.Test;

public class ClamsTest {

    private Class<?> clamsClass;

    @Before
    public void setUp() throws Exception {
        clamsClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1."
                + "factory.clam.Clams");
    }

    @Test
    public void testClamsIsAPublicInterface() {
        int classModifiers = clamsClass.getModifiers();

        assertTrue(Modifier.isInterface(classModifiers));
        assertTrue(Modifier.isPublic(classModifiers));
    }

    @Test
    public void testClamsHasToStringAbstractMethod() throws Exception {
        Method toString = clamsClass.getDeclaredMethod("toString");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
    }
}