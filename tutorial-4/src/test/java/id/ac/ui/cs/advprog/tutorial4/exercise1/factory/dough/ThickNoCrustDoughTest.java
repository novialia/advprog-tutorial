package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class ThickNoCrustDoughTest {
    private ThickNoCrustDough thicknoCrustDough;

    @Before
    public void setUp() {
        thicknoCrustDough = new ThickNoCrustDough();
    }

    @Test
    public void testToString() {
        assertEquals("ThickNoCrust style extra thick no crust dough", thicknoCrustDough.toString());
    }
}
