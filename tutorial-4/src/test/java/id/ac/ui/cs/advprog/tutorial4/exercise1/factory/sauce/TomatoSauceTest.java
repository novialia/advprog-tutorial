package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class TomatoSauceTest {
    private TomatoSauce tomatoSauce;

    @Before
    public void setUp() {
        tomatoSauce = new TomatoSauce();
    }

    @Test
    public void testToString() {
        assertEquals("Tomato Sauce", tomatoSauce.toString());
    }
}
