package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

public class ThickNoCrustDough implements Dough {
    public String toString() {
        return "ThickNoCrust style extra thick no crust dough";
    }
}

