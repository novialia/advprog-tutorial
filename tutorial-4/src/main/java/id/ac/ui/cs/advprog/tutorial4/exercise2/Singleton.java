package id.ac.ui.cs.advprog.tutorial4.exercise2;

public class Singleton {
    private static Singleton instance;

    private Singleton() {
        instance = null;
    }

    public static Singleton getInstance() {
        if (instance == null) {
            instance = new Singleton();
        }
        return instance;
    }
}