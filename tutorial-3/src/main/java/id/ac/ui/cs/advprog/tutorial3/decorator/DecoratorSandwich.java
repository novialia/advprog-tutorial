package id.ac.ui.cs.advprog.tutorial3.decorator;

import id.ac.ui.cs.advprog.tutorial3.decorator.bread.BreadProducer;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.BeefMeat;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.ChickenMeat;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.ChiliSauce;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.Tomato;

public class DecoratorSandwich {

    public static void main(String[] args) {

        //Crusty Sandwich with Beef Meat, Chicken Meat, Using Tomato and Chili Sauce
        BreadProducer doubleBeefChickenDoubleSauceSandwich = BreadProducer.CRUSTY_SANDWICH;
        Food crustyMeatSandwich = doubleBeefChickenDoubleSauceSandwich.createBreadToBeFilled();
        Food food = new ChiliSauce(new Tomato(new ChickenMeat(new BeefMeat(crustyMeatSandwich))));
        System.out.println("Description: " + food.getDescription());
        System.out.println("Cost: " + food.cost());

    }

}


