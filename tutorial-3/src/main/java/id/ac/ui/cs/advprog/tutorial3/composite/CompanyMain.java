package id.ac.ui.cs.advprog.tutorial3.composite;

import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Ceo;
import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Cto;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.BackendProgrammer;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.FrontendProgrammer;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.NetworkExpert;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.SecurityExpert;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.UiUxDesigner;

public class CompanyMain {

    public static void main(String[] args) {
        Company higherups = new Company();
        Company techexpert = new Company();

        Employees ceo = new Ceo("Crocodile", 500000.00);
        Employees cto = new Cto("Alvida", 320000.00);
        higherups.addEmployee(ceo);
        higherups.addEmployee(cto);

        Employees backend = new BackendProgrammer("Kuma", 94000.00);
        Employees frontend = new FrontendProgrammer("Hancock",66000.00);
        Employees uiUxDesigner = new UiUxDesigner("Rayleigh", 177000.00);
        Employees networkExpert = new NetworkExpert("Aokiji", 83000.00);
        Employees securityExpert = new SecurityExpert("Kizaru", 100000);
        techexpert.addEmployee(backend);
        techexpert.addEmployee(frontend);
        techexpert.addEmployee(uiUxDesigner);
        techexpert.addEmployee(networkExpert);
        techexpert.addEmployee(securityExpert);

    }
}

